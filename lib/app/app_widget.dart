import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: Modular.navigatorKey,
      title: 'Moviapp',
      initialRoute: '/',
      onGenerateRoute: Modular.generateRoute,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          color: Color(0xFF34adb4),
        ),
      ),
    );
  }
}
