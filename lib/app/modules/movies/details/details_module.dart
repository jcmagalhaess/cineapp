import 'package:flutter_modular/flutter_modular.dart';

import 'details_page.dart';

class DetailsMovieModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<Router> get routers => [
        Router('/:movie', child: (_, args) => DetailsMoviePage(movie: args.params['movie'])),
        // Router('/:originalTitle', child: (_, args) => DetailsMoviePage(originalTitle: args.params['originalTitle'],)),
      ];

  static Inject get to => Inject<DetailsMovieModule>.of();
}
