import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:slidy_cli/app/modules/movies/models/movie_models.dart';
import 'package:slidy_cli/app/modules/movies/movies_controller.dart';

class DetailsMoviePage extends StatefulWidget {
  final List<MovieModel> movie;

  const DetailsMoviePage({
    Key key,
    this.movie
  }) : super(key: key);

  @override
  _DetailsMoviePageState createState() => _DetailsMoviePageState();
}

class _DetailsMoviePageState extends State<DetailsMoviePage> {  
  @override
  Widget build(BuildContext context) {
  final moviesController = Modular.get<MoviesController>();
    // print(moviesController);
    // print(moviesController.movie);
    return Scaffold(
      appBar: AppBar(
        title: Text(moviesController.movie.toString()),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text(moviesController.movie.hashCode.toString()),
            Text(moviesController.category.toString()),            
            // Text(moviesController.movie.id.toString()),
            // Text(moviesController.movie.popularity.toString()),
            // Text(moviesController.movie.voteCount.toString()),
            // Text(moviesController.movie.voteAverage.toString()),
            // Text(moviesController.movie.adult.toString()),
            // Text(moviesController.movie.video.toString()),
            // Text(moviesController.movie.posterPath),
            // Text(moviesController.movie.overview),
            // Text(moviesController.movie.releaseDate),
            // Text(moviesController.movie.originalLanguage.toString()),
            // Text(moviesController.movie.backdropPath),
          ],
        ),
      ),
    );
  }
}
