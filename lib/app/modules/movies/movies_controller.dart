import 'package:mobx/mobx.dart';
import 'package:slidy_cli/app/modules/movies/models/movie_models.dart';
import 'package:slidy_cli/app/modules/movies/repositories/movie_repository.dart';

part 'movies_controller.g.dart';

class MoviesController = _MoviesControllerBase with _$MoviesController;

abstract class _MoviesControllerBase with Store {
  Object movie;

  String category;
  final MovieRepository repository;

  @observable
  ObservableFuture<List<MovieModel>> movies;

  @override
  _MoviesControllerBase({this.repository, this.category}) {
    fetchMovies();
  }

  @action
  fetchMovies() {
    movies = repository.getAllMovies(category).asObservable();
  }
}
