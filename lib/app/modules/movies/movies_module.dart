import 'package:flutter_modular/flutter_modular.dart';
import 'package:dio/dio.dart';
import 'package:slidy_cli/app/modules/movies/repositories/movie_repository.dart';
import 'package:slidy_cli/app/shared/utils/constants.dart';

import 'movies_controller.dart';
import 'movies_page.dart';

class MoviesModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => MoviesController(repository: i.get<MovieRepository>())),
        Bind((i) => MovieRepository(dio: i.get<Dio>())),
        Bind((i) => Dio(BaseOptions(baseUrl: URL_BASE))),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => MoviesPage()),
      ];
}
