import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:slidy_cli/app/modules/movies/movies_controller.dart';
import 'package:slidy_cli/app/widgets/bottom_navbar/bottom_navbar_widget.dart';
import 'package:slidy_cli/app/widgets/poster_list/poster_list_widget.dart';

import '../../widgets/poster_list/poster_list_widget.dart';

class MoviesPage extends StatefulWidget {
  @override
  _MoviesPageState createState() => _MoviesPageState();
}

class _MoviesPageState extends State<MoviesPage> {
  final moviesController = Modular.get<MoviesController>();

  @override
  Widget build(BuildContext context) {
    print(moviesController.movies.value);
    return Scaffold(
      appBar: AppBar(
        title: Text('Movies'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(children: <Widget>[
          PosterListWidget(
            tag: Text('Popular',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            category: 'popular',
          ),
          PosterListWidget(
            tag: Text('Top Rated',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            category: 'top_rated',
          ),
        ]),
      ),
      bottomNavigationBar: BottomNavbarWidget(),
    );
  }
}
