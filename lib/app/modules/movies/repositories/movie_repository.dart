import 'package:dio/dio.dart';
import 'package:slidy_cli/app/modules/movies/models/movie_models.dart';
import 'package:slidy_cli/app/shared/utils/constants.dart';

class MovieRepository {
  final Dio dio;
  
  MovieRepository({this.dio});

  Future<List<MovieModel>> getAllMovies(String category) async {
    var response = await dio.get('/movie/' + category + API_KEY + LANGUAGE);
    List<MovieModel> list = [];
    // print(category);
    for (var json in (response.data['results'] as List)) {
      MovieModel model = MovieModel(
        id: json['id'],
        originalTitle: json['original_title'],
        title: json['title'],
        posterPath: json['poster_path'],
      );
      list.add(model);
    }

    return list;
  }
}
