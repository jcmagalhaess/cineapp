import 'package:dio/dio.dart';
import 'package:slidy_cli/app/modules/home/models/genre_models.dart';
import 'package:slidy_cli/app/shared/utils/constants.dart';

class GenreRepository {
  final Dio dio;

  GenreRepository(this.dio);

  Future<List<GenreModel>> getAllGenres() async {
    var response = await dio.get('/genre/list' + API_KEY + LANGUAGE);
    List<GenreModel> list = [];
    
    for (var json in (response.data['genres'] as List)) {
      GenreModel model = GenreModel(name: json['name']);
      list.add(model);
    }
    
    return list;
  }

}