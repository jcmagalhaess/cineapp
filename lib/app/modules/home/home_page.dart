import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:slidy_cli/app/widgets/bottom_navbar/bottom_navbar_widget.dart';

import 'home_controller.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final homeController = Modular.get<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Genêros'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.cast),
            onPressed: null,
            tooltip: 'Transmitir',
          )
        ],
      ),
      body: Observer(
        builder: (_) {
          if (homeController.genres.error != null) {
            return Center(
              child: RaisedButton(
                color: Colors.orange[800],
                child: Text("Pressione Novamente"),
                onPressed: () {
                  homeController.fetchGenres();
                },
              ),
            );
          }

          if (homeController.genres.value == null) {
            return Center(child: CircularProgressIndicator());
          }

          var list = homeController.genres.value;

          return ListView.builder(
            itemCount: list.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Text(list[index].name),
                onTap: () {
                  homeController.name = list[index].name.toString(); 
                  // Navigator.pushNamed(context, '/details/${homeController.name}');
                  Modular.to.pushNamed('/detailsGenre/${homeController.name}');
                  // print(homeController.name);
                },
              );
            },
          );
        },
      ),
      bottomNavigationBar: BottomNavbarWidget(),
    );
  }
}
