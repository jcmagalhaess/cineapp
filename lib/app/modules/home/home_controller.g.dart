// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$genresAtom = Atom(name: '_HomeControllerBase.genres');

  @override
  ObservableFuture<List<GenreModel>> get genres {
    _$genresAtom.context.enforceReadPolicy(_$genresAtom);
    _$genresAtom.reportObserved();
    return super.genres;
  }

  @override
  set genres(ObservableFuture<List<GenreModel>> value) {
    _$genresAtom.context.conditionallyRunInAction(() {
      super.genres = value;
      _$genresAtom.reportChanged();
    }, _$genresAtom, name: '${_$genresAtom.name}_set');
  }

  final _$_HomeControllerBaseActionController =
      ActionController(name: '_HomeControllerBase');

  @override
  dynamic fetchGenres() {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction();
    try {
      return super.fetchGenres();
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }
}
