import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:slidy_cli/app/modules/home/home_controller.dart';

class DetailsGenrePage extends StatefulWidget {
  final String name;

  const DetailsGenrePage({Key key, this.name}) : super(key: key);

  @override
  _DetailsGenrePageState createState() => _DetailsGenrePageState();
}

class _DetailsGenrePageState extends State<DetailsGenrePage> {
  @override
  Widget build(BuildContext context) {
    final homeController = Modular.get<HomeController>();
    print(homeController);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name),
      ),
      body: Center(
        child: Text(homeController.name),
      ),
    );
  }
}
