import 'package:flutter_modular/flutter_modular.dart';

import 'details_page.dart';

class DetailsGenreModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<Router> get routers => [
        Router('/:name', child: (_, args) => DetailsGenrePage(name: args.params['name'],)),
      ];

  static Inject get to => Inject<DetailsGenreModule>.of();
}
