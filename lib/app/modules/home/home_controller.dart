import 'package:mobx/mobx.dart';
import 'package:slidy_cli/app/modules/home/models/genre_models.dart';
import 'package:slidy_cli/app/modules/home/repositories/genre_repository.dart';
part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  String name;

  final GenreRepository repository;

  @observable
  ObservableFuture<List<GenreModel>> genres;

  _HomeControllerBase(this.repository) {
    fetchGenres();
  }

  @action
  fetchGenres() {
    genres = repository.getAllGenres().asObservable();
  }
}