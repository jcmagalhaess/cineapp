import 'package:slidy_cli/app/modules/home/details/details_module.dart';
import 'package:slidy_cli/app/modules/movies/details/details_module.dart';
import 'package:slidy_cli/app/widgets/poster_list/poster_list_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:slidy_cli/app/modules/movies/movies_module.dart';
import 'package:slidy_cli/app/widgets/bottom_navbar/bottom_navbar_controller.dart';
import 'package:slidy_cli/app/app_widget.dart';
import 'package:slidy_cli/app/app_controller.dart';
import 'package:slidy_cli/app/modules/home/home_module.dart';
import 'package:slidy_cli/app/modules/login/login_module.dart';
import 'package:slidy_cli/app/modules/splash/splash_page.dart';
import 'package:slidy_cli/app/modules/splash/splash_controller.dart';

class AppModule extends MainModule {
  String category;


  @override
  List<Bind> get binds => [
        Bind((i) => PosterListController()),
        Bind((i) => BottomNavbarController()),
        Bind((i) => SplashController()),
        Bind((i) => AppController()),
      ];

  @override
  List<Router> get routers => [
        Router('/splash', child: (_, args) => SplashPage()),
        Router('/login', module: LoginModule()),
        Router('/home', module: HomeModule()),
        Router('/detailsMovie', module: DetailsMovieModule()),
        Router('/detailsGenre', module: DetailsGenreModule()),
        Router('/', module: MoviesModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
