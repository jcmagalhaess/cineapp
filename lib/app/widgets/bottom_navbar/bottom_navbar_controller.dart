import 'package:mobx/mobx.dart';

part 'bottom_navbar_controller.g.dart';

class BottomNavbarController = _BottomNavbarBase with _$BottomNavbarController;

abstract class _BottomNavbarBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
