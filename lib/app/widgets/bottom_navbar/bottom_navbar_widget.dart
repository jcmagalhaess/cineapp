import 'package:flutter/material.dart';

import 'navigation_item.dart';

class BottomNavbarWidget extends StatefulWidget {
  @override
  _BottomNavbarWidgetState createState() => _BottomNavbarWidgetState();
}

class _BottomNavbarWidgetState extends State<BottomNavbarWidget> {
  Widget currentRoute;
  int currentPosition = 0;

  List<NavigationItem> items = [
    NavigationItem(
      Icon(Icons.home),
      Text('Home'),
      Color(0xFF257B80),
      '/home',
    ),
    NavigationItem(
      Icon(Icons.local_movies),
      Text('Movies'),
      Color(0xFF257B80),
      '/movies',
    ),
    NavigationItem(
      Icon(Icons.person_pin),
      Text('Actors'),
      Color(0xFF257B80),
      '/actors',
    ),
    NavigationItem(
      Icon(Icons.person_outline),
      Text('Profile'),
      Color(0xFF257B80),
      '/profile',
    ),
  ];

  Widget _buildItem(NavigationItem item, bool isSelected) {
    return AnimatedContainer(
      duration: Duration(seconds: 1),
      curve: Curves.fastLinearToSlowEaseIn,
      width: isSelected ? 100 : 40,
      height: double.maxFinite,
      decoration: isSelected
          ? BoxDecoration(
              color: item.color, borderRadius: BorderRadius.circular(30))
          : null,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8),
                child: IconTheme(
                  child: item.icon,
                  data: IconThemeData(
                      size: 24,
                      color: isSelected ? Colors.white : Color(0xFF34adb4)),
                ),
              ),
              isSelected
                  ? Padding(
                      padding: const EdgeInsets.only(left: 7),
                      child: DefaultTextStyle.merge(
                        style: TextStyle(color: Colors.white),
                        child: item.label,
                      ),
                    )
                  : Container()
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: FractionallySizedBox(
        widthFactor: 0.95,
        child: Container(
          height: 56,
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 10,
            )
          ], color: Colors.white, borderRadius: BorderRadius.circular(30)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: items.map((item) {
              var itemIndex = items.indexOf(item);
              return InkWell(
                onTap: () {
                  setState(() {
                    currentPosition = itemIndex;
                  });
                  Future.delayed(Duration(seconds: 1)).then((v) {
                    Navigator.pushReplacementNamed(context, item.route);
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _buildItem(item, currentPosition == itemIndex),
                ),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
