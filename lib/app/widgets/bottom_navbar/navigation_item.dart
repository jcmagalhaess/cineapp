import 'package:flutter/material.dart';

class NavigationItem {
  final Icon icon;
  final Text label;
  final Color color;
  final String route;

  NavigationItem(this.icon, this.label, this.color, this.route);
}