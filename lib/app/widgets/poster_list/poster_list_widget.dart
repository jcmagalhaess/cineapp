import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:slidy_cli/app/modules/movies/movies_controller.dart';
import 'package:slidy_cli/app/shared/utils/constants.dart';

import '../../modules/movies/models/movie_models.dart';

class PosterListWidget extends StatefulWidget {
  final Text tag;
  final String title;
  final String category;
  final Object movie;

  const PosterListWidget(
      {Key key, this.title, this.category, this.tag, this.movie})
      : super(key: key);

  @override
  _PosterListWidgetState createState() => _PosterListWidgetState();
}

class _PosterListWidgetState extends State<PosterListWidget> {
  @override
  Widget build(BuildContext context) {
    final moviesController = Modular.get<MoviesController>();

    moviesController.category = this.widget.category;

    return Observer(builder: (_) {
      if (moviesController.movies.error != null) {
        return Center(
          child: FloatingActionButton(
            splashColor: Colors.pink,
            backgroundColor: Color(0xFF34adb4),
            child: Icon(Icons.refresh),
            onPressed: () {
              moviesController.fetchMovies();
            },
          ),
          /*child: InkWell(
            borderRadius: BorderRadius.circular(30),
            child: RaisedButton(
              color: Color(0xFF34adb4),
              textColor: Colors.white,
              child: Icon(Icons.refresh, color: Colors.white),
              onPressed: () {
                moviesController.fetchMovies();
              },
            ),
          ),*/
        );
      }

      if (moviesController.movies.value == null) {
        return Center(child: CircularProgressIndicator());
      }

      var list = moviesController.movies.value;

      return Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: double.infinity,
              child: widget.tag,
            ),
          ),
          Container(
            width: double.infinity,
            height: 150,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                var urlPoster = URL_IMG + list[index].posterPath;
                // print(list[index].title);
                return InkWell(
                  child: Tooltip(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Color(0xFF34adb4)),
                    height: 50,
                    message: list[index].title,
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 2),
                        child: Image.network(urlPoster),
                      ),
                    ),
                  ),
                  onTap: () {
                    moviesController.movie = list[index];
                    Modular.to
                        .pushNamed('/detailsMovie/${moviesController.movie}');
                  },
                );
              },
            ),
          ),
        ],
      );
    });
  }
}
