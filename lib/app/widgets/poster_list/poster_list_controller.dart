import 'package:mobx/mobx.dart';

part 'poster_list_controller.g.dart';

class PosterListController = _PosterListBase with _$PosterListController;

abstract class _PosterListBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
