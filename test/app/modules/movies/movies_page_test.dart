import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:slidy_cli/app/modules/movies/movies_page.dart';

main() {
  testWidgets('MoviesPage has title', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(MoviesPage(title: 'Movies')));
    final titleFinder = find.text('Movies');
    expect(titleFinder, findsOneWidget);
  });
}
