import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:slidy_cli/app/modules/movies/movies_controller.dart';
import 'package:slidy_cli/app/modules/movies/movies_module.dart';

void main() {
  initModule(MoviesModule());
  MoviesController movies;

  setUp(() {
    movies = MoviesModule.to.get<MoviesController>();
  });

  group('MoviesController Test', () {
    test("First Test", () {
      expect(movies, isInstanceOf<MoviesController>());
    });

    test("Set Value", () {
      expect(movies.value, equals(0));
      movies.increment();
      expect(movies.value, equals(1));
    });
  });
}
