import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:slidy_cli/app/modules/details/details_page.dart';

main() {
  testWidgets('DetailsPage has title', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(DetailsPage(title: 'Details')));
    final titleFinder = find.text('Details');
    expect(titleFinder, findsOneWidget);
  });
}
