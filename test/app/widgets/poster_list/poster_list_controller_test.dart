import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:slidy_cli/app/widgets/poster_list/poster_list_controller.dart';
import 'package:slidy_cli/app/app_module.dart';

void main() {
  initModule(AppModule());
  PosterListController posterlist;

  setUp(() {
    posterlist = AppModule.to.get<PosterListController>();
  });

  group('PosterListController Test', () {
    test("First Test", () {
      expect(posterlist, isInstanceOf<PosterListController>());
    });

    test("Set Value", () {
      expect(posterlist.value, equals(0));
      posterlist.increment();
      expect(posterlist.value, equals(1));
    });
  });
}
