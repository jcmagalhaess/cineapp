import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:slidy_cli/app/widgets/poster_list/poster_list_widget.dart';

main() {
  testWidgets('PosterListWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(PosterListWidget()));
    final textFinder = find.text('PosterList');
    expect(textFinder, findsOneWidget);
  });
}
