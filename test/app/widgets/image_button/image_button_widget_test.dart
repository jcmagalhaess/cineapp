import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:slidy_cli/app/widgets/image_button/image_button_widget.dart';

main() {
  testWidgets('ImageButtonWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(ImageButtonWidget()));
    final textFinder = find.text('ImageButton');
    expect(textFinder, findsOneWidget);
  });
}
