import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:slidy_cli/app/widgets/image_button/image_button_controller.dart';
import 'package:slidy_cli/app/app_module.dart';

void main() {
  initModule(AppModule());
  ImageButtonController imagebutton;

  setUp(() {
    imagebutton = AppModule.to.get<ImageButtonController>();
  });

  group('ImageButtonController Test', () {
    test("First Test", () {
      expect(imagebutton, isInstanceOf<ImageButtonController>());
    });

    test("Set Value", () {
      expect(imagebutton.value, equals(0));
      imagebutton.increment();
      expect(imagebutton.value, equals(1));
    });
  });
}
