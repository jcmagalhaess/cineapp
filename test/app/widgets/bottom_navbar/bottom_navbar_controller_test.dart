import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:slidy_cli/app/widgets/bottom_navbar/bottom_navbar_controller.dart';
import 'package:slidy_cli/app/app_module.dart';

void main() {
  initModule(AppModule());
  BottomNavbarController bottomnavbar;

  setUp(() {
    bottomnavbar = AppModule.to.get<BottomNavbarController>();
  });

  group('BottomNavbarController Test', () {
    test("First Test", () {
      expect(bottomnavbar, isInstanceOf<BottomNavbarController>());
    });

    test("Set Value", () {
      expect(bottomnavbar.value, equals(0));
      bottomnavbar.increment();
      expect(bottomnavbar.value, equals(1));
    });
  });
}
