import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:slidy_cli/app/widgets/bottom_navbar/bottom_navbar_widget.dart';

main() {
  testWidgets('BottomNavbarWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(BottomNavbarWidget()));
    final textFinder = find.text('BottomNavbar');
    expect(textFinder, findsOneWidget);
  });
}
